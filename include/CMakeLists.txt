add_library( headerlib-comms INTERFACE )
# Use of aliases of the form namespace::component is considered good practice, to avoid confusing error
# messages if the lib is not available as a target. CMake will then tack it on to the linker command line
# with -l, which can make the user believe there should exist a libheaderlib.so file somewhere.
# -lrpany::headerlib doesn't make sense as the name of a .so file, so easier to diagnose errors
# Not sure that headerlib is a good name though, but need to differentiate it from the binary target rpany
add_library( rpany::comms::headerlib-comms ALIAS headerlib-comms )

# Attaching these settings to a target rather than having them as global settings. Then whatever client
# that depends on this target will get the stuff added automatically to the compile/link commands
target_include_directories(headerlib-comms INTERFACE $<BUILD_INTERFACE:${CMAKE_CURRENT_SOURCE_DIR}> $<INSTALL_INTERFACE:${RPANY_PREFIX}/include>)
target_link_libraries( headerlib-comms INTERFACE rpany::headerlib ${CMAKE_THREAD_LIBS_INIT} )

install( DIRECTORY "${CMAKE_CURRENT_SOURCE_DIR}/rpany" DESTINATION include FILES_MATCHING PATTERN "*.hpp" )
install( TARGETS headerlib-comms EXPORT rpany-comms-targets )
