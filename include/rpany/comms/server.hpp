#pragma once

namespace server {

using namespace boost;
using boost::asio::ip::tcp;

typedef boost::system::error_code error_code;

typedef std::shared_ptr< asio::ip::tcp::iostream > stream_ptr;

class session : public std::enable_shared_from_this< session >
{
	public:
		session( const rpany::stack_ptr &stack_, stream_ptr stream_, const asio::ip::tcp::endpoint &endpoint_ )
		: stream( stream_ )
		, stack( stack_ )
		, endpoint( endpoint_ )
		{
		}

		~session( )
		{
			if ( !runner.joinable( ) ) return;
			if ( *stream )
			{
				*stream << std::endl;
				stream->socket( ).cancel( );
				stream->socket( ).shutdown( tcp::socket::shutdown_both );
				if ( stack && !stack->closed( ) )
					stack->close( );
			}
			runner.join( );
		}

		void start( )
		{
			if ( !runner.joinable( ) )
				runner = std::thread( [ this ]( ) { run( ); } );
		}

		bool is_dead( )
		{
			bool result = false;
			if ( mutex.try_lock( ) )
			{
				result = !running || !*stream || stream->error( );
				mutex.unlock( );
			}
			return result;
		}

		stream_ptr get_stream( ) const
		{
			return stream;
		}

	protected:
		void run( )
		{
			try
			{
				run_loop( );
			}
			catch( ... )
			{
				// ignore for now
			}

			stop( );
		}

		void run_loop( )
		{
			if ( is_running( ) )
			{
				*stack << "[[" << "$" << "__main__" << "find-name" << "]]" << "ss-eval-list";
				bool has_main = false;
				*stack << "ss-pull" >> has_main;
				if ( has_main )
				{
					std::unique_lock< std::mutex > lock( mutex );
					*stack << "[[" << "__main__" << "]]" << "ss-eval-list";
					running = false;
				}
			}

			while( is_running( ) )
			{
				std::string command;
				std::getline( *stream, command );
				if ( is_running( ) )
				{
					std::unique_lock< std::mutex > lock( mutex );
					*stack << "[[" << command << "]]" << "ss-eval-list";
				}
				std::this_thread::yield( );
			}
		}

		void stop( )
		{
			std::unique_lock< std::mutex > lock( mutex );
			if ( stack && !stack->closed( ) )
				stack->close( );
			running = false;
		}

		bool is_running( )
		{
			std::unique_lock< std::mutex > lock( mutex );
			return stack && running && *stream;
		}

	private:
		stream_ptr stream;
		std::thread runner;

		rpany::stack_ptr stack;
		const asio::ip::tcp::endpoint endpoint;

		std::mutex mutex;
		bool running = true;
};

class handler
{
	public:
		typedef std::shared_ptr< session > session_ptr;
		typedef std::vector< session_ptr > session_list;

		handler( rpany::stack_ptr &s, asio::io_service &io_service_, const asio::ip::tcp::endpoint &endpoint )
		: stack( s )
		, io_service( io_service_ )
		, acceptor( io_service, endpoint )
		{
			do_accept( );
		}

		size_t size( ) const
		{
			std::unique_lock< std::mutex > lock( mutex );
			return active.size( );
		}

		void clean( )
		{
			std::unique_lock< std::mutex > lock( mutex );
			for ( auto iter = active.begin( ); iter != active.end( ); )
			{
				if ( ( *iter )->is_dead( ) )
					iter = active.erase( iter );
				else
					iter ++;
			}
		}

		void kill( )
		{
			std::unique_lock< std::mutex > lock( mutex );
			acceptor.close( );
			io_service.stop( );
		}

	private:
		// Sets up the asynchronous accept mechanism
		void do_accept( )
		{
			stream = stream_ptr( new asio::ip::tcp::iostream );
			acceptor.async_accept( *stream->rdbuf( ), peer_endpoint, [ this ]( error_code ec ) { if ( run_accept( ec ) ) do_accept( ); } );
		}

		// Executed when a new connection is presented
		bool run_accept( error_code ec )
		{
			bool result = !ec;
			if ( result )
				add( std::make_shared< session >( factory( ), stream, peer_endpoint ) );
			return result;
		}

		rpany::stack_ptr factory( )
		{
			rpany::stack_ptr ss = rpany::factory::create( stack );
			ss->set_input( *stream );
			ss->set_output( *stream );
			ss->set_error( *stream );
			try
			{
				*ss << std::to_string( id ++ ) << "local" << "session-id";
				*ss << "$" << peer_endpoint.address( ).to_string( ) << "local" << "session-ip";
				*ss << std::to_string( peer_endpoint.port( ) ) << "local" << "session-port";
				*ss << "server-factory";
				return ss;
			}
			catch( const std::exception &e )
			{
				*stream << "ERROR: " << e.what( ) << std::endl;
				return rpany::stack_ptr( );
			}
			return rpany::stack_ptr( );
		}

		void add( const session_ptr &session )
		{
			session->start( );
			std::unique_lock< std::mutex > lock( mutex );
			active.push_back( session );
		}

		rpany::stack_ptr stack;
		asio::io_service &io_service;
		asio::ip::tcp::acceptor acceptor;
		asio::ip::tcp::endpoint peer_endpoint;
		stream_ptr stream;

		mutable std::mutex mutex;
		session_list active;

		int id = 0;
};

struct server_type
{
	server_type( rpany::stack_ptr &s, int port_ = 57708 )
	: stack( s )
	, port( port_ )
	, endpoint( asio::ip::tcp::v4( ), port )
	, server( stack, io_service, endpoint )
	, thread( [ this ]( ) { io_service.run( ); } )
	{ }

	~server_type( )
	{
		if ( !thread.joinable( ) ) return;
		io_service.stop( );
		thread.join( );
	}

	void clean( )
	{
		server.clean( );
	}

	void kill( )
	{
		server.kill( );
	}

	rpany::stack_ptr stack;
	int port;
	asio::io_service io_service;
	asio::ip::tcp::endpoint endpoint;
	handler server;
	std::thread thread;
};

typedef std::shared_ptr< server_type > server_ptr;

inline void run( rpany::stack &s )
{
	auto producer( rpany::factory::create( s ) );
	s += server_ptr( new server_type( producer ) );
}

inline void run_custom( rpany::stack &s )
{
	auto producer( rpany::factory::create( s ) );
	s += server_ptr( new server_type( producer, s.pull< int >( ) ) );
}

inline void clients( rpany::stack &s )
{
	auto &server = s.cast< server_ptr >( );
	s += server->server.size( );
}

inline void port( rpany::stack &s )
{
	auto &server = s.cast< server_ptr >( );
	s += server->port;
}

inline void clean( rpany::stack &s )
{
	auto &server = s.cast< server_ptr >( );
	server->clean( );
}

inline void kill( rpany::stack &s )
{
	auto server = s.pull< server_ptr >( );
	server->kill( );
}

inline rp::any server_to_string( const rp::any &any )
{
	auto server = rpany::as< server_ptr >( any );
	std::ostringstream os;
	os << "<server-" << server->port << ">";
	return os.str( );
}

RPANY_OPEN( server, rpany::stack &s )
	s.teach( "server-start", run );
	s.teach( "server-run", run_custom );
	s.teach( "server-clients", clients );
	s.teach( "server-port", port );
	s.teach( "server-clean", clean );
	s.teach( "server-kill", kill );
	s.teach( "server-factory", { "ss-new", "ss-educate-all" } );
	rpany::coercion( COERCE_KEY( server_ptr, std::string ), server_to_string );
RPANY_CLOSE( server );

}
