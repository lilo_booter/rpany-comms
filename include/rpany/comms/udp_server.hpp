#pragma once

namespace udp_server {

using boost::asio::ip::udp;

class server
{
	public:
		typedef std::shared_ptr< server > ptr;

		server( int port_ )
		: port( port_ )
		, socket_( io_service, udp::endpoint( udp::v4( ), port ) )
		{
		}

		boost::asio::io_service io_service;
		int port;

		void start( rpany::stack &s )
		{
			socket_.async_receive_from( boost::asio::buffer( buffer_ ), endpoint_,
				[ & ]( const boost::system::error_code &ec, std::size_t length )
				{
					if ( ec ) return;
					buffer_[ length ] = '\0';

					s += std::string( buffer_.data( ) );
					s << "udp-server-recv";

					auto info = s.pull< std::string >( );

					if ( info.size( ) == 0 )
					{
						start( s );
						return;
					}

					socket_.async_send_to( boost::asio::buffer( info.c_str( ), info.size( ) ), endpoint_,
						[ & ]( const boost::system::error_code &ec, std::size_t length )
						{
							if ( ec ) return;
							start( s );
						} );
				} );
		}

		udp::socket socket_;
		udp::endpoint endpoint_;
		std::array< char, 10240 > buffer_;
};

inline void server_start( rpany::stack &s )
{
	int port = s.pull< int >( );
	s += server::ptr( new server( port ) );
}

inline void run( rpany::stack &s )
{
	auto server = s.cast< server::ptr >( );
	server->start( s );
	server->io_service.run( );
}

inline void run_one( rpany::stack &s )
{
	auto server = s.cast< server::ptr >( );
	server->start( s );
	server->io_service.run_one( );
}

inline void recv( rpany::stack &s )
{
	auto info = s.pull< std::string >( );
	s.output( ) << info;
	s += std::string( "" );
}

inline rp::any server_to_string( const rp::any &any )
{
	auto server = rpany::as< server::ptr >( any );
	std::ostringstream os;
	os << "<udp-server-" << server->port << ">";
	return os.str( );
}

RPANY_OPEN( udp_server, rpany::stack &s )
	s.teach( "udp-server-start", server_start );
	s.teach( "udp-server-run", run );
	s.teach( "udp-server-recv", recv );
	rpany::coercion( COERCE_KEY( server::ptr, std::string ), server_to_string );
RPANY_CLOSE( udp_server );

}
