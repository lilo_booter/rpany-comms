#pragma once

namespace client {

using boost::asio::ip::tcp;

const std::string newline( "\n" );
const std::string space( " " );

class connection
{
	public:
		connection( const std::string &server_ = "localhost", int port_ = 57708 )
		: server( server_ )
		, port( port_ )
		{
			connect( );
		}

		~connection( )
		{
		}

		std::string name( ) const
		{
			return server + ":" + std::to_string( port );
		}

		tcp::iostream &get_stream( )
		{
			return stream;
		}

		void set_delim( char ch )
		{
			delim = ch;
		}

		void send( const std::string &data )
		{
			stream << data;
		}

		void send_delim( )
		{
			stream << delim;
		}

		std::string receive( )
		{
			std::string result;
			boost::asio::socket_base::bytes_readable command( true );
			stream.socket( ).io_control( command );
			if ( command.get( ) > 0 )
				std::getline( stream, result );
			return result;
		}

		std::string readline( )
		{
			std::string result;
			std::getline( stream, result, delim );
			return result;
		}

	private:
		void connect( )
		{
			stream = tcp::iostream( server, std::to_string( port ) );
		}

		const std::string server;
		const int port;
		char delim = '\n';

		tcp::iostream stream;
};

typedef std::shared_ptr< connection > connection_ptr;

inline void connect_at( rpany::stack &s )
{
	auto port = s.pull< int >( );
	auto server = s.pull< std::string >( );
	s += connection_ptr( new connection( server, port ) );
}

inline void connect_local( rpany::stack &s )
{
	auto port = s.pull< int >( );
	s += connection_ptr( new connection( "localhost", port ) );
}

inline void client_delim( rpany::stack &s )
{
	int ch = s.pull< int >( );
	auto &connection = s.cast< connection_ptr >( );
	connection->set_delim( static_cast< char >( ch ) );
}

inline void send_commands( rpany::stack &s )
{
	auto vector = s.pull< rpany::vector_ptr >( );
	auto &connection = s.cast< connection_ptr >( );

	for ( auto it = vector->begin( ); it != vector->end( ); it ++ )
		connection->send( rpany::quoted( rpany::convert< std::string >( *it ) ) + " " );
	connection->send_delim( );
}

inline void send( rpany::stack &s )
{
	auto str = s.pull< std::string >( );
	auto &connection = s.cast< connection_ptr >( );
	connection->send( str );
	connection->send_delim( );
}

inline void readline( rpany::stack &s )
{
	auto &connection = s.cast< connection_ptr >( );
	s += connection->readline( );
}

inline void receive_reply( rpany::stack &s )
{
	auto &connection = s.cast< connection_ptr >( );
	s += connection->receive( );
}

inline void client_good( rpany::stack &s )
{
	auto &connection = s.cast< connection_ptr >( );
	s += connection->get_stream( ).good( );
}

inline void client_fail( rpany::stack &s )
{
	auto &connection = s.cast< connection_ptr >( );
	s += connection->get_stream( ).fail( );
}

inline void client_eof( rpany::stack &s )
{
	auto &connection = s.cast< connection_ptr >( );
	s += connection->get_stream( ).eof( );
}

inline void client_bad( rpany::stack &s )
{
	auto &connection = s.cast< connection_ptr >( );
	s += connection->get_stream( ).bad( );
}

inline rp::any connection_to_string( const rp::any &any )
{
	return std::string( "<connection>" );
}

RPANY_OPEN( client, rpany::stack &s )
	s.teach( "client-connect", connect_at );
	s.teach( "client-local", connect_local );
	s.teach( "client-good", client_good );
	s.teach( "client-fail", client_fail );
	s.teach( "client-eof", client_eof );
	s.teach( "client-bad", client_bad );
	s.teach( "client-delim", client_delim );
	s.teach( "client-readline", readline );
	s.teach( "client-send", send );
	s.teach( "client-send-commands", send_commands );
	s.teach( "client-receive", receive_reply );
	rpany::coercion( COERCE_KEY( connection_ptr, std::string ), connection_to_string );
RPANY_CLOSE( client );

}
