#pragma once

namespace udp_client {

using boost::asio::ip::udp;

class connection
{
	public:
		connection( const std::string &host = "localhost", int port = 57708 )
		: m_host( host )
		, m_port( port )
		, m_socket( m_io_context, udp::endpoint( udp::v4( ), 0 ) )
		{
			udp::resolver resolver( m_io_context );
			udp::resolver::query query( udp::v4( ), host, std::to_string( port ) );
			udp::resolver::iterator iter = resolver.resolve( query );
			m_endpoint = *iter;
		}

		~connection( )
		{
			m_socket.close( );
		}

		std::string name( ) const
		{
			return m_host + ":" + std::to_string( m_port );
		}

		void set_delim( char ch )
		{
			m_delim = ch;
		}

		void send( const std::string &msg )
		{
			m_socket.send_to( boost::asio::buffer( msg, msg.size( ) ), m_endpoint );
		}

		std::string recv( )
		{
			m_socket.wait( boost::asio::ip::tcp::socket::wait_read );
			m_socket.receive( boost::asio::buffer( m_buffer.data( ), m_buffer.size( ) ) );
			return std::string( m_buffer.data( ), m_buffer.size( ) );
		}

		char delim( )
		{
			return m_delim;
		}

	private:
		const std::string m_host;
		const int m_port;
		char m_delim = '\n';
		std::array< char, 10240 > m_buffer;

		boost::asio::io_context m_io_context;
		boost::asio::ip::udp::socket m_socket;
		boost::asio::ip::udp::endpoint m_endpoint;
};

typedef std::shared_ptr< connection > connection_ptr;

inline void connect_at( rpany::stack &s )
{
	auto port = s.pull< int >( );
	auto host = s.pull< std::string >( );
	s += connection_ptr( new connection( host, port ) );
}

inline void connect_local( rpany::stack &s )
{
	auto port = s.pull< int >( );
	s += connection_ptr( new connection( "localhost", port ) );
}

inline void client_delim( rpany::stack &s )
{
	int ch = s.pull< int >( );
	auto &connection = s.cast< connection_ptr >( );
	connection->set_delim( static_cast< char >( ch ) );
}

inline void send( rpany::stack &s )
{
	auto str = s.pull< std::string >( );
	auto &connection = s.cast< connection_ptr >( );
	connection->send( str + connection->delim( ) );
}

inline void recv( rpany::stack &s )
{
	auto &connection = s.cast< connection_ptr >( );
	s += connection->recv( );
}

inline rp::any connection_to_string( const rp::any &any )
{
	return std::string( "<udp-client>" );
}

RPANY_OPEN( udp_client, rpany::stack &s )
	s.teach( "udp-connect", connect_at );
	s.teach( "udp-local", connect_local );
	s.teach( "udp-delim", client_delim );
	s.teach( "udp-send", send );
	s.teach( "udp-recv", recv );
	rpany::coercion( COERCE_KEY( connection_ptr, std::string ), connection_to_string );
RPANY_CLOSE( udp_client );

}
