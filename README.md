# rpany-comms

A mixin for [rpany] which provides a multi-user rpany server.

## Usage

To get a list of everything rpany-comms knows:

```
rpany-comms
```

See below for the details on the specific vocabs and words which are introduced.

To start a server instance:

```
$ rpany-comms /usr/local/bin/rpany-shell
> server-start
>
```

The shell is still usable, and the stack holds the server object. If you remove it,
the server is destroyed and all clients are disconnected. The shell is fully usable
otherwise (you can also also assign the server to a variable to keep it active).

To connect to and send commands to a server:

```
$ rpany-comms /usr/local/bin/rpany-shell
> client-connect
> { 1 2 3 * + . } client-send-commands
> client-receive .
7
```

This can also be done, multiple times even, from the shell running the server.

To activate as a mixin with another rpany project, it is necessary to include
the `rpany-comms.hpp` header in the same file as `rpany-host.hpp`

```
#include <rpany/rpany-host.cpp>
#include <rpany/rpany-comms.cpp>
```

And introduce it in your CMakeLists.txt like:

```
find_package( rpany-comms REQUIRED )
target_link_libraries( YOUR-TARGET PRIVATE rpany::comms::headerlib-comms )
```

Once recompiled, the hosting executables will be cognisant of the rpany-comms
vocabularies.

If you wish to make the communications optional, use a compile time macro like:

```
#include <rpany/rpany-host.cpp>

#if defined( WITH_RPANY_COMMS )
#include <rpany/rpany-comms.cpp>
#endif
```

with the corresponding setup in cmake:

```
find_package( rpany-comms )
if ( rpany-comms_FOUND )
	target_compile_definitions( YOUR-TARGET PRIVATE WITH_RPANY_COMMS )
	target_link_libraries( YOUR-TARGET PRIVATE rpany::comms::headerlib-comms )
endif( )
```

## Prequisites

* [cmake]
* c++11 compiler
* [rpany]
* [boost::asio]

## Optional

* [rpany-linenoise-ng]

## Details

This mixin provides a general mechanism to provide multi user, multi threaded
access to the hosting applications rpany instance.

Each connection has its own thread and stack and can work independently of the
others, accepting tokens understood by the hosting application, or a subset of
those vocabularies if required.

## Vocabularies

Accepts all `tokens` as per hosting application.

### `server`

Create instances of the server using this vocabulary.

| Word             | Usage                 | Result     | Comment                               |
| ---------------- | --------------------- | ---------- | ------------------------------------- |
| `server-start`   | `server-start`        | `<server>` | Starts a server on port 57708         |
| `server-run`     | `<port> server-run`   | `<server>` | Starts a server on the specified port |

What a server does is controlled by the `server-factory` word. By default, this
is defined as:

`: server-factory ss-new ss-educate-all ;`

A client connecting to this will be provided with a raw socket to evaluate
commands and is ideal for process to process communications.

However, any script can be instantiated by way of the factory too, hence to
offer the user a standard rpany-shell on port 8000:

```
> : server-factory ss-new ss-educate-all [[ study shell.rp ]] ss-eval-list ;
> 8000 server-run
```

A user can then connect on that port using socate, netcat or telnet or similar
tools.

```
$ socat - TCP4:localhost:8000
>
```

Additionally, it is safe to rewrite and server-factory while a server is running
as each server retains a complete copy of the dictionary at the point the server
is instantiated. Hence we can offer different types of services on different ports
from a single instance of rpany-comms.

What is executed is controlled by the substacks `__main__` word, hence, we can
lock the user out of the stack evaluation loop entirely:

```
> : server-factory ss-new ss-educate-all [[ : __main__ study extra.rp 100 guess ; ]] ss-eval-list ;
> 8001 server-run
```

### `client`

Create instances of a TCP client (designed for implementing simple synchronus protocols such as HTTP, MVCP etc).

| Word             | Usage                          | Result         | Comment                                             |
| ---------------- | ------------------------------ | -------------- | --------------------------------------------------- |
| `client-connect` | `<host> <post> client-connect` | `<connection>` | Create a connection to a server at host:port        |
| `client-local`   | `<port>` `client-local`        | `<connection>` | Creates a connection to a server on localhost:port  |

Connection state:

| Word          | Usage                      | Result                | Comment                                             |
| ------------- | -------------------------- | --------------------- | --------------------------------------------------- |
| `client-good` | `<connection> client-good` | `<connection> <bool>` | TOS is true if connection is good                   |
| `client-fail` | `<connection> client-fail` | `<connection> <bool>` | TOS is true if connection has failed                |
| `client-eof`  | `<connection> client-eof`  | `<connection> <bool>` | TOS is true if connection has eof'd                 |
| `client-bad`  | `<connection> client-bad`  | `<connection> <bool>` | TOS is true if connection is bad                    |

Connection modifiers:

| Word           | Usage                              | Result         | Comment                                               |
| -------------- | ---------------------------------- | ---------------| ----------------------------------------------------- |
| `client-delim` | `<connection> <char> client-delim` | `<connection>` | Inputs and outputs are terminated with the char given |

Send commands to the server:

| Word                   | Usage                                 | Result         | Comment                                       |
| ---------------------- | ------------------------------------- | -------------- | --------------------------------------------- |
| `client-send`          | `<connection> <string>                | `<connection>` | Sends a comand to the server                  |

Read responses:

| Word              | Usage                          | Result                       | Comment                                                  |
| ----------------- | ------------------------------ | ---------------------------- | -------------------------------------------------------- |
| `client-readline` | `<connection> client-readline` | `<connection> <string>`      | Inputs and outputs are terminated with the char given    |

Alternatively, use a tool like netcat to connect like:

```
$ nc localhost 57708
help
```

[cmake]: https://cmake.org/
[rpany]: https://gitlab.com/lilo_booter/rpany/blob/master/README.md
[rpany-linenoise-ng]: https://gitlab.com/lilo_booter/rpany-linenoise-ng
[boost::asio]: https://www.boost.org/doc/libs/1_69_0/libs/asio/
